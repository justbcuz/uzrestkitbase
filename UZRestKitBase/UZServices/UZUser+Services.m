//
//  UZUser+Services.m
//  RestKitTest
//
//  Created by Justin Leger on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZUser+Services.h"
#import "UZServiceManager.h"


@implementation UZUser (Services)


#pragma mark - User Service Methods

+ (void) userWithKey:(NSString *)aKey successBlock:(void(^)(UZUser *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/users/%@", aKey];
	
	//__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			UZUser * user = (UZUser *) [dictionary objectForKey:@"user"];
			aSuccessBlock(user);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

#pragma mark - User-Friend Service Methods

- (void) friendsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{	
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
    NSString * userKey = [self isMemberOfClass:[UZUserSelf class]] ? @"self" : self.identifier;
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/users/%@/friends", userKey];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			NSArray * arrayResults = (NSArray *) [dictionary objectForKey:@"friends"];
			aSuccessBlock(arrayResults);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

- (void) friendRequestsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{	
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
    NSString * userKey = [self isMemberOfClass:[UZUserSelf class]] ? @"self" : self.identifier;
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/users/%@/requests", userKey];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			NSArray * arrayResults = (NSArray *) [dictionary objectForKey:@"requests"];
			aSuccessBlock(arrayResults);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
	
}


#pragma mark - Bout Service Methods

- (void) activitiesWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
    NSString * userKey = [self isMemberOfClass:[UZUserSelf class]] ? @"self" : self.identifier;
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/users/%@/targets", userKey];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			NSArray * arrayResults = (NSArray *) [dictionary objectForKey:@"targets"];
			aSuccessBlock(arrayResults);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}


#pragma mark - Target Service Methods

- (void) targetsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
    NSString * userKey = [self isMemberOfClass:[UZUserSelf class]] ? @"self" : self.identifier;
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/users/%@/bouts", userKey];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			NSArray * arrayResults = (NSArray *) [dictionary objectForKey:@"bouts"];
			aSuccessBlock(arrayResults);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

- (void) challengesWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
    NSString * userKey = [self isMemberOfClass:[UZUserSelf class]] ? @"self" : self.identifier;
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/users/%@/challenges", userKey];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			NSArray * arrayResults = (NSArray *) [dictionary objectForKey:@"challenges"];
			aSuccessBlock(arrayResults);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

#pragma mark - Comment Service Methods

- (void) commentsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
    NSString * userKey = [self isMemberOfClass:[UZUserSelf class]] ? @"self" : self.identifier;
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/users/%@/comments", userKey];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			NSArray * arrayResults = (NSArray *) [dictionary objectForKey:@"comments"];
			aSuccessBlock(arrayResults);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

@end
