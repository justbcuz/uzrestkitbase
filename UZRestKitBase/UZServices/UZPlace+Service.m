//
//  UZPlace+Service.m
//  UrbanZombie
//
//  Created by Justin Leger on 4/17/12.
//  Copyright (c) 2012 Leger Incorporated. All rights reserved.
//

#import "UZPlace+Service.h"
#import "UZServiceManager.h"

@implementation UZPlace (Service)

#pragma mark - User Service Methods

+ (void) placeWithKey:(NSString *)aKey successBlock:(void(^)(UZPlace *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth place key
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/place/%@", aKey];
	
	//__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			UZPlace * place = (UZPlace *) [dictionary objectForKey:@"place"];
			aSuccessBlock(place);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

#pragma mark - Near by Places Service Methods

+ (void) nearbyPlacesAtLocation:(CLLocation *)aLocation withinRadius:(NSInteger)aRadius successBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{	
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
//	
//	NSNumber * latitude = [NSNumber numberWithDouble:aLocation.coordinate.latitude];
//	NSNumber * longitude = [NSNumber numberWithDouble:aLocation.coordinate.longitude];
	NSNumber * latitude = [NSNumber numberWithDouble:32.83559];
	NSNumber * longitude = [NSNumber numberWithDouble:-96.677277];
	NSNumber * radius = [NSNumber numberWithInteger:aRadius];
	NSMutableDictionary * queryParams = [[NSMutableDictionary alloc] initWithObjectsAndKeys:latitude, @"lat", longitude, @"lng", nil];
	if ([radius intValue] > 0) [queryParams setObject:radius forKey:@"radius"];
	
	NSString * resourcePath = [@"/v1/places/nearby" stringByAppendingQueryParameters:queryParams];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			NSArray * arrayResults = (NSArray *) [dictionary objectForKey:@"places"];
			aSuccessBlock(arrayResults);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
	
	[queryParams release];
}


#pragma mark - Comment Service Methods

- (void) commentsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
    NSString * userKey = [self isMemberOfClass:[UZUserSelf class]] ? @"self" : self.identifier;
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/users/%@/comments", userKey];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			NSArray * friends = (NSArray *) [dictionary objectForKey:@"comments"];
			aSuccessBlock(friends);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

@end
