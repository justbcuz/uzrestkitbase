//
//  UZBout+Service.m
//  UrbanZombie
//
//  Created by Justin Leger on 4/17/12.
//  Copyright (c) 2012 Leger Incorporated. All rights reserved.
//

#import "UZBout+Service.h"
#import "UZServiceManager.h"


@implementation UZBout (Service)

#pragma mark - User Service Methods

+ (void) boutWithKey:(NSString *)aKey successBlock:(void(^)(UZUser *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/bout/%@", aKey];
	
	//__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			UZBout * bout = (UZBout *) [dictionary objectForKey:@"bout"];
			aSuccessBlock(bout);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

#pragma mark - Comment Service Methods

- (void) commentsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/bout/%@/comments", self.identifier];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			NSArray * arrayResults = (NSArray *) [dictionary objectForKey:@"comments"];
			aSuccessBlock(arrayResults);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

@end
