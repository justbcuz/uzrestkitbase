//
//  UZServiceManager.h
//  RestKitTest
//
//  Created by Justin Leger on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

#import "UZObjectMappingProvider.h"

#import "SynthesizeSingleton.h"

@interface UZServiceManager : NSObject

SYNTHESIZE_SINGLETON_FOR_CLASS_HEADER(UZServiceManager);

@property (nonatomic, retain) RKObjectManager * manager;
@property (nonatomic, retain) NSString * userAuthToken;

@property (nonatomic, assign) BOOL userAuthTokenEnabled;
@property (nonatomic, assign) BOOL apiKeyEnabled;

#pragma mark - Public Class Callback Methods

+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObjectsDictionary:(NSDictionary *)dictionary successBlock:(void(^)(NSDictionary *))aSuccessBlock;
+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObject:(id)object successBlock:(void(^)(id))aSuccessBlock;
+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObjects:(NSArray *)objects successBlock:(void(^)(NSArray *))aSuccessBlock;
+ (void) objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error failureBlock:(void(^)(NSError *))aFailureBlock;

@end
