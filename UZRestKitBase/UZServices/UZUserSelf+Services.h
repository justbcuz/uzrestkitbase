//
//  UZUser+Services.h
//  RestKitTest
//
//  Created by Justin Leger on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UZUserSelf.h"

@interface UZUserSelf (Services)

+ (void) loginWithUsername:(NSString*) aUsername andPassword:(NSString *) aPassword successBlock:(void(^)(NSDictionary *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

+ (void) loginWithAuthToken:(NSString*) aAuthToken successBlock:(void(^)(NSDictionary *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) saveWithSuccessBlock:(void(^)(UZUserSelf *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

@end
