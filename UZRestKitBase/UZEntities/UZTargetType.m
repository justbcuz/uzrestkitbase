//
//  UZTargetType.m
//  UZDataModel
//
//  Created by Justin Leger on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZTargetType.h"
#import "UZTarget.h"

@implementation UZTargetType

@synthesize type;
@synthesize respawnDuration;
@synthesize speedFactor;
@synthesize targets;

#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping * _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZTargetType Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZTargetType class]] retain];
        //_mapping.primaryKeyAttribute = @"identifier";
        
        [_mapping mapKeyPathsToAttributes:
         
		 @"key", @"identifier",
		 @"inserted", @"createdDate",
		 @"updated", @"updatedDate",
		 
		 @"type", @"type",
		 @"respawnDuration", @"respawnDuration",
		 @"speedFactor", @"speedFactor",
         
         nil];
		
        [_mapping mapRelationship:@"targets" withMapping:[UZTarget mapping]];
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{   
    [super initialize];
    
    [UZTargetType mapping];
}

@end
