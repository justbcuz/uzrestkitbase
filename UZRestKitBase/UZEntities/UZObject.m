//
//  UZObject.m
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZObject.h"

@implementation UZObject

#pragma mark -
#pragma mark RestKit Mapping Builder

+ (RKObjectMapping *) mapping {
    NSAssert(FALSE, @"Child Classes needs to override this method.");
    return nil;
}

@end
