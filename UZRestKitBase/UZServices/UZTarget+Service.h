//
//  UZTarget+Service.h
//  UrbanZombie
//
//  Created by Justin Leger on 4/17/12.
//  Copyright (c) 2012 Leger Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "UZTarget.h"

@interface UZTarget (Service)

+ (void) targetWithKey:(NSString *)aKey successBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) commentsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) placeAtPlace:(UZPlace *)aPlace forUser:(UZUser *)aUser successBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;
- (void) placeAtPlaceKey:(NSString *)aPlaceKey forUserKey:(NSString *)aUserKey successBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) pickupAtLoction:(CLLocation *)aLocation successBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;
- (void) reclaimWithSuccessBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) updateWithSuccessBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

@end
