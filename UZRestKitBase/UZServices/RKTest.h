//
//  RKTest.h
//  UZDataModel
//
//  Created by Justin Leger on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RKTest : NSObject

-(void) runRKTests;

@end
