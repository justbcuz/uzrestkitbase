//
//  UZBoutTarget.m
//  UZDataModel
//
//  Created by Justin Leger on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZBoutTarget.h"

@implementation UZBoutTarget

@synthesize boutKey;
@synthesize targetKey;
@synthesize resultType;
@synthesize result;
@synthesize duration;
@synthesize targetPointValue;
@synthesize xLoc;
@synthesize yLoc;
@synthesize zLoc;

@synthesize killedUser;
@synthesize userHitCount;
@synthesize headHitCount;
@synthesize torsoHitCount;
@synthesize armHitCount;
@synthesize legHitCount;
@synthesize lifespan;
@synthesize remainingHealth;
@synthesize crippledAtDeath;
@synthesize pointsEarned;

@synthesize bout;
@synthesize target;

#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping * _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZBoutTarget Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZBoutTarget class]] retain];
        //_mapping.primaryKeyAttribute = @"identifier";
        
        [_mapping mapKeyPathsToAttributes:
         
		 @"key", @"identifier",
		 @"inserted", @"createdDate",
		 @"updated", @"updatedDate",
		 
		 @"boutKey", @"boutKey",
		 @"targetKey", @"targetKey",
		 @"resultType", @"resultType",
		 @"result", @"result",
		 @"duration", @"duration",
		 @"targetPointValue", @"targetPointValue",
		 @"xLoc", @"xLoc",
		 @"yLoc", @"yLoc",
		 @"zLoc", @"zLoc",
		 
		 @"killedUser", @"killedUser",
		 @"hits", @"userHitCount",
		 @"headHitCount", @"headHitCount",
		 @"torsoHitCount", @"torsoHitCount",
		 @"armHitCount", @"armHitCount",
		 @"legHitCount", @"legHitCount",
		 @"lifespan", @"lifespan",
		 @"remainingHealth", @"remainingHealth",
		 @"crippledAtDeath", @"crippledAtDeath",
		 @"pointsEarned", @"pointsEarned",
         
         nil];
		
		[_mapping mapRelationship:@"bout" withMapping:[UZBout mapping]];
		[_mapping mapRelationship:@"target" withMapping:[UZTarget mapping]];
		
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{   
    [super initialize];
    
    [UZBoutTarget mapping];
}

@end
