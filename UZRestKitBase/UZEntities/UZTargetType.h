//
//  UZTargetType.h
//  UZDataModel
//
//  Created by Justin Leger on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZValueObject.h"

@interface UZTargetType : UZValueObject

@property (nonatomic, retain) NSString* type;
@property (nonatomic, retain) NSNumber* respawnDuration;
@property (nonatomic, retain) NSNumber* speedFactor;

#pragma mark -
#pragma mark Relationships

@property (nonatomic, retain) NSSet* targets;

@end
