//
//  UZObjectMappingProvider.m
//  UZDataModel
//
//  Created by Justin Leger on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZObjectMappingProvider.h"

#pragma mark - Private methods declaration
@interface UZObjectMappingProvider ()

@end

@implementation UZObjectMappingProvider

- (id)init {
    self = [super init];
	if (self) {
		
//		Removed the root meta key
//		[self setObjectMapping:[UZMeta mapping] forKeyPath:@"meta"];
		
		[self setObjectMapping:[UZUserSelf mapping] forKeyPath:@"self"];
		
		[self setObjectMapping:[UZUser mapping] forKeyPath:@"friends"];
		[self setObjectMapping:[UZUser mapping] forKeyPath:@"friend"];
		[self setObjectMapping:[UZUser mapping] forKeyPath:@"users"];
		[self setObjectMapping:[UZUser mapping] forKeyPath:@"user"];
		
		[self setObjectMapping:[UZPlace mapping] forKeyPath:@"places"];
		[self setObjectMapping:[UZPlace mapping] forKeyPath:@"place"];
        
		[self setObjectMapping:[UZTarget mapping] forKeyPath:@"targets"];
		[self setObjectMapping:[UZTarget mapping] forKeyPath:@"target"];
		
		[self setObjectMapping:[UZBout mapping] forKeyPath:@"bouts"];
		[self setObjectMapping:[UZBout mapping] forKeyPath:@"bout"];
		
		[self setObjectMapping:[UZFriendRequest mapping] forKeyPath:@"friendRequests"];
		[self setObjectMapping:[UZFriendRequest mapping] forKeyPath:@"friendRequest"];
		
//		Removed the root meta key
//		self.errorMapping = [UZErrorMeta mapping];

    }
	return self;
}

#pragma mark - Private Methods

@end
