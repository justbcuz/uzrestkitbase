//
//  UZUser+Services.m
//  RestKitTest
//
//  Created by Justin Leger on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZUserSelf+Services.h"
#import "UZUser+Services.h"
#import "UZServiceManager.h"

#pragma mark - Private Methods Declaration
@interface UZUserSelf ()

+ (void) loginWithDictionary:(NSMutableDictionary*) aDictionary successBlock:(void(^)(NSDictionary *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

@end

@implementation UZUserSelf (Services)

- (void) saveWithSuccessBlock:(void(^)(UZUserSelf *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
	
	[manager putObject:self usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			UZUserSelf * user = (UZUserSelf *) [dictionary objectForKey:@"self"];
			aSuccessBlock(user);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

#pragma mark - Service Methods
#pragma mark - Login Service Methods

+ (void) loginWithUsername:(NSString*) aUsername andPassword:(NSString *) aPassword successBlock:(void(^)(NSDictionary *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
    // Define the extra query string parameters
    NSMutableDictionary * requestParams = [[NSMutableDictionary alloc] init]; 
    
    // Append the query string parameters to the URL
    [requestParams setValue:aUsername forKey:@"un"];
    [requestParams setValue:aPassword forKey:@"pw"];
    
    [UZUserSelf loginWithDictionary:requestParams successBlock:aSuccessBlock failureBlock:aFailureBlock];
    
    [requestParams release];
}

+ (void) loginWithAuthToken:(NSString*) aAuthToken successBlock:(void(^)(NSDictionary *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{	
	[UZServiceManager sharedInstance].userAuthToken = aAuthToken;
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    
    [UZUserSelf loginWithDictionary:nil successBlock:aSuccessBlock failureBlock:aFailureBlock];
    
   // [requestParams release];
}

#pragma mark - Private Methods

+ (void) loginWithDictionary:(NSMutableDictionary*) aDictionary successBlock:(void(^)(NSDictionary *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
	NSString * resourcePath;
	
	if (aDictionary) {
		resourcePath = [@"/v1/users/login" stringByAppendingQueryParameters:aDictionary];
	} else {
		[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
		resourcePath = @"/v1/users/login";
	}
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        //[loader.mappingProvider registerObjectMapping:[UZResponse mappingWithKeyPath:@"user" andMapping:[UZUserSelf mapping]] withRootKeyPath:@""];
        
        // Define the Callback blocks
        
		[loader.additionalHTTPHeaders setValue:[[UIDevice currentDevice] uniqueIdentifier]  forKey:@"udid"];
		
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
            [UZServiceManager objectLoader:loader didLoadObjectsDictionary:dictionary successBlock:aSuccessBlock];
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

@end
