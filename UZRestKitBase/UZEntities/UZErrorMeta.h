//
//  UZErrorMeta.h
//  UZDataModel
//
//  Created by Justin Leger on 3/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZObject.h"

@interface UZErrorMeta : UZObject

@property (nonatomic, retain) NSString* code;
@property (nonatomic, retain) NSString* type;
@property (nonatomic, retain) NSString* message;

@end
