//
//  UZBout.m
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZBout.h"
#import "UZBoutTarget.h"

@implementation UZBout

@synthesize userKey;
@synthesize placeKey;
@synthesize boutRequestKey;
@synthesize status;
@synthesize startDate;
@synthesize duration;
@synthesize userPointValue;
@synthesize targetCount;
@synthesize timeout;

@synthesize formula;

@synthesize killCount;
@synthesize headHitCount;
@synthesize torsoHitCount;
@synthesize armHitCount;
@synthesize legHitCount;
@synthesize remainingHealth;
@synthesize totalHits;
@synthesize totalMisses;
@synthesize totalShots;
@synthesize pointsEarned;

@synthesize user;
@synthesize place;
@synthesize boutTargets;


#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping * _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZBout Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZBout class]] retain];
        //_mapping.primaryKeyAttribute = @"identifier";
        
        [_mapping mapKeyPathsToAttributes:
         
         @"key", @"identifier",
         @"inserted", @"createdDate",
         @"updated", @"updatedDate",
         
         @"userKey", @"userKey",
         @"placeKey", @"placeKey",
         @"boutRequestKey", @"boutRequestKey",
         @"status", @"status",
         @"startDate", @"startDate",
         @"duration", @"duration",
         @"userPointValue", @"userPointValue",
         @"targetCount", @"targetCount",
         @"timeout", @"timeout",
		 
		 @"formula", @"formula",
		 
		 @"kills", @"killCount",
		 @"headHitCount", @"headHitCount",
		 @"torsoHitCount", @"torsoHitCount",
		 @"armHitCount", @"armHitCount",
		 @"legHitCount", @"legHitCount",
		 @"remainingHealth", @"remainingHealth",
		 @"totalHits", @"totalHits",
		 @"totalMisses", @"totalMisses",
		 @"totalShots", @"totalShots",
		 @"pointsEarned", @"pointsEarened",
         
         nil];
        
        [_mapping mapRelationship:@"user" withMapping:[UZUser mapping]];
        [_mapping mapRelationship:@"place" withMapping:[UZPlace mapping]];
        [_mapping mapRelationship:@"boutTargets" withMapping:[UZBoutTarget mapping]];
        
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [super initialize];
    
    [UZBout mapping];
}

@end
