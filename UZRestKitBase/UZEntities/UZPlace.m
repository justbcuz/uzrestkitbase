//
//  UZPlace.m
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZPlace.h"
#import "UZTarget.h"
#import "UZBout.h"

@implementation UZPlace

@synthesize fsKey;
@synthesize categoryKey;
@synthesize fsCategoryKey;

@synthesize name;
@synthesize phone;
@synthesize address;
@synthesize city;
@synthesize state;
@synthesize zipcode;
@synthesize country;

@synthesize latitude;
@synthesize longitude;
@synthesize distance;

@synthesize boutCount;
@synthesize targetCount;

@synthesize category;
@synthesize fsCategory;

@synthesize bouts;
@synthesize targets;


#pragma mark -
#pragma mark Convinience getters

@synthesize location;
- (NSString *) location {
	
	NSString * result = @"";
	if (self.city) result = [result stringByAppendingString:self.city];
	if (self.state) result = [result stringByAppendingString:[NSString stringWithFormat:@", %@", self.state]];
	if (self.country) result = [result stringByAppendingString:[NSString stringWithFormat:@", %@", self.country]];
	
	return result;
}


#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping * _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZPlace Mapping");
    
        _mapping = [[RKObjectMapping mappingForClass:[UZPlace class]] retain];
        //_mapping.primaryKeyAttribute = @"identifier";

// TODO: Ass killCOunt winCount lossCount
		
        [_mapping mapKeyPathsToAttributes:
         
             @"key", @"identifier",
             @"inserted", @"createdDate",
             @"updated", @"updatedDate",
             
             @"fsKey", @"fsKey",
             
             @"categoryKey", @"categoryKey",
             @"fsCategoryKey", @"fsCategoryKey",
             
             @"name", @"name",
             @"phone", @"phone",
             @"address", @"address",
             @"city", @"city",
             @"stateCode", @"state",
             @"zipcode", @"zipcode",
             @"countryCode", @"country",
             
             @"lat", @"latitude",
             @"lng", @"longitude",
             
             @"distance", @"distance",
             @"boutCount", @"boutCount",
             @"targetCount", @"targetCount",
         
         nil];
        
        [_mapping mapRelationship:@"category" withMapping:[UZCategory mapping]];
        [_mapping mapRelationship:@"fsCategory" withMapping:[FSCategory mapping]];
		
        [_mapping mapRelationship:@"bouts" withMapping:[UZBout mapping]];
        [_mapping mapRelationship:@"targets" withMapping:[UZTarget mapping]];
        
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [super initialize];
    
    [UZPlace mapping];
}



@end
