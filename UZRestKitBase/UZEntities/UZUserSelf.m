//
//  UZUserSelf.m
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZUserSelf.h"

@implementation UZUserSelf

@synthesize password;
@synthesize email;
@synthesize authToken;

@synthesize loginCount;
@synthesize previousLoginDate;


#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping * _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZUserSelf Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZUserSelf class]] retain];
        //_mapping.primaryKeyAttribute = @"identifier";
        
        [_mapping mapKeyPathsToAttributes:
         
			 @"key", @"identifier",
			 @"inserted", @"createdDate",
			 @"updated", @"updatedDate",
			 
			 @"pw", @"password",
			 @"email", @"email",
			 @"authToken", @"authToken",
			 
			 @"un", @"username",
			 @"gender", @"gender",
			 
			 @"city", @"city",
			 @"state", @"state",
			 @"country", @"country",
			 
			 @"thumbSmall", @"thumbSmall",
			 @"thumbMedium", @"thumbMedium",
			 @"thumbLarge", @"thumbLarge",
			 
			 @"skill", @"skillLevel",
			 @"cooldown", @"checkinCooldown",
			 @"boutLength", @"maxBoutLength",
			 @"kill", @"killCount",
			 @"win", @"winCount",
			 @"loss", @"lossCount",
			 @"flee", @"fleeCount",
			 
			 @"headHitCount", @"headHitCount",
			 @"torsoHitCount", @"torsoHitCount",
			 @"armHitCount", @"armHitCount",
			 @"legHitCount", @"legHitCount",
			 @"totalHits", @"totalHits",
			 @"totalMisses", @"totalMisses",
			 
			 @"accuracy", @"accuracy",
			 
			 @"total", @"totalPoints",
			 @"current", @"curentPoints",
         
         nil];
        
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [super initialize];
    
    [UZUserSelf mapping];
}

@end
