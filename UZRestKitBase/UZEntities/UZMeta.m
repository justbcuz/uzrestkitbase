//
//  UZMeta.m
//  RestKitTest
//
//  Created by Justin Leger on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZMeta.h"

@implementation UZMeta

@synthesize code;
@synthesize type;
@synthesize message;

#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping * _mapping = nil;
//static RKObjectMapping * _errorMapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZMeta Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZMeta class]] retain];
        
        [_mapping mapKeyPathsToAttributes:
         
		 @"code", @"code",
		 @"type", @"type",
		 @"message", @"message",
         
         nil];
        
    }
    
    return _mapping;
}

//+ (RKObjectMapping *) errorMapping {
//    
//    if (!_errorMapping) {
//        
//        NSLog(@"I am building The Error UZMeta Mapping");
//        
//        _errorMapping = [[RKObjectMapping mappingForClass:[UZMeta class]] retain];
//        
//        [_mapping mapKeyPathsToAttributes:
//         
//		 @"meta.code", @"code",
//		 @"meta.type", @"type",
//		 @"meta.message", @"message",
//         
//         nil];
//        
//    }
//    
//    return _errorMapping;
//}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [super initialize];
    
    [UZMeta mapping];
//    [UZMeta errorMapping];
}



@end
