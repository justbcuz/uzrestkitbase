//
//  UZErrorMeta.m
//  UZDataModel
//
//  Created by Justin Leger on 3/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZErrorMeta.h"

@implementation UZErrorMeta

@synthesize code;
@synthesize type;
@synthesize message;

#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping * _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZErrorMeta Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZErrorMeta class]] retain];
        
        [_mapping mapKeyPathsToAttributes:
         
		 @"meta.code", @"code",
		 @"meta.type", @"type",
		 @"meta.message", @"message",
         
         nil];
        
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [super initialize];
    
    [UZErrorMeta mapping];
}



@end