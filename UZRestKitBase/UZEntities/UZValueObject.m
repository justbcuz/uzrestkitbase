//
//  UZValueObject.m
//  RestKitTest
//
//  Created by Justin Leger on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZValueObject.h"

@implementation UZValueObject

@synthesize identifier;
@synthesize createdDate;
@synthesize updatedDate;

#pragma mark -
#pragma mark RestKit Mapping Builder

+ (RKObjectMapping *) mapping {
    NSAssert(FALSE, @"Child Classes needs to override this method.");
    return nil;
}

@end
