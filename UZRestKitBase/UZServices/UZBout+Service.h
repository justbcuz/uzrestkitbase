//
//  UZBout+Service.h
//  UrbanZombie
//
//  Created by Justin Leger on 4/17/12.
//  Copyright (c) 2012 Leger Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UZBout.h"

@interface UZBout (Service)

+ (void) boutWithKey:(NSString *)aKey successBlock:(void(^)(UZUser *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) commentsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

@end
