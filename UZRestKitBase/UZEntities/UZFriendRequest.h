//
//  UZFriendRequest.h
//  RestKitTest
//
//  Created by Justin Leger on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZValueObject.h"
#import "UZUser.h"

@interface UZFriendRequest : UZValueObject

@property (nonatomic, retain) NSString* userKey;
@property (nonatomic, retain) NSString* requestedUserKey;
@property (nonatomic, retain) NSString* confirmationState;
@property (nonatomic, retain) NSString* confirmationKey;

#pragma mark -
#pragma mark Relationships

@property (nonatomic, retain) UZUser* user;
@property (nonatomic, retain) UZUser* requestedUser;

@end
