//
//  UZBout.h
//  UZDataModel
//
//  Created by Justin Leger on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZValueObject.h"

#import "UZUser.h"
#import "UZPlace.h"

@interface UZBout : UZValueObject

@property (nonatomic, retain) NSString* userKey;
@property (nonatomic, retain) NSString* placeKey;
@property (nonatomic, retain) NSString* boutRequestKey;
@property (nonatomic, retain) NSString* status;
@property (nonatomic, retain) NSDate* startDate;
@property (nonatomic, retain) NSNumber* duration;
@property (nonatomic, retain) NSNumber* userPointValue;
@property (nonatomic, retain) NSNumber* targetCount;
@property (nonatomic, retain) NSNumber* timeout;

@property (nonatomic, retain) NSString* formula;

@property (nonatomic, retain) NSNumber* killCount;
@property (nonatomic, retain) NSNumber* headHitCount;
@property (nonatomic, retain) NSNumber* torsoHitCount;
@property (nonatomic, retain) NSNumber* armHitCount;
@property (nonatomic, retain) NSNumber* legHitCount;
@property (nonatomic, retain) NSNumber* remainingHealth;
@property (nonatomic, retain) NSNumber* totalHits;
@property (nonatomic, retain) NSNumber* totalMisses;
@property (nonatomic, retain) NSNumber* totalShots;
@property (nonatomic, retain) NSNumber* pointsEarned;

#pragma mark -
#pragma mark Relationships

@property (nonatomic, retain) UZUser* user;
@property (nonatomic, retain) UZPlace* place;
@property (nonatomic, retain) NSSet* boutTargets;

@end
