//
//  UZServiceManager.m
//  RestKitTest
//
//  Created by Justin Leger on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZServiceManager.h"

#define USER_AUTH_TOKEN @"DYGiTSBQhl5iO1aSFe2zh9fTWXzVyYohXYuosWg1IFJxgKqqSRM7vSVomSLiR2uMZ5"
#define API_AUTH_KEY @"DYGiTSBQhl5iO1aSFe2zh9fTWXzVyYohXYuosWg1IFJxgKqqSRM7vSVomSLiR2uMZ5"

#define USER_AUTH_HEADER_VALUE_KEY @"at"
#define API_HEADER_VALUE_KEY @"apiKey"

#pragma mark - Private methods declaration
@interface UZServiceManager ()
- (void)initializeRestKit;
@end


@implementation UZServiceManager

SYNTHESIZE_SINGLETON_FOR_CLASS(UZServiceManager);

@synthesize manager;
@synthesize userAuthToken = _userAuthToken;
@synthesize userAuthTokenEnabled = _userAuthTokenEnabled;
@synthesize apiKeyEnabled = _apiKeyEnabled;

- (id) init {
    self = [super init];
	if (self) {
        
        [self initializeRestKit];
		self.userAuthTokenEnabled = YES;
		self.apiKeyEnabled = YES;
		
		//self.userAuthToken = USER_AUTH_TOKEN;
		
		self.manager.mappingProvider = [[[UZObjectMappingProvider alloc] init] autorelease];
    }
    
	return self;
}

#pragma mark - Private methods

- (void)initializeRestKit {
    
    // Configure RestKit logging
    RKLogConfigureByName("RestKit", RKLogLevelTrace);
    RKLogConfigureByName("RestKit/Network", RKLogLevelDebug);
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelDebug);
    RKLogConfigureByName("RestKit/Network/Queue", RKLogLevelDebug);
    // RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
    
    // Define the shared objectManager
	//self.manager = [RKObjectManager objectManagerWithBaseURLString:@"http://api.urbanzombie.dev"];
    self.manager = [RKObjectManager objectManagerWithBaseURLString:@"http://legerinc.no-ip.org:8888"];
    [self.manager.requestQueue setShowsNetworkActivityIndicatorWhenBusy:YES];
	[self.manager.client.HTTPHeaders setValue:@"" forKey:@"apiKey"];
    
    // Setup the MIME types
    self.manager.acceptMIMEType = RKMIMETypeJSON;
    self.manager.serializationMIMEType = RKMIMETypeJSON;
}

- (void)setUserAuthTokenEnabled:(BOOL)userAuthTokenEnabled
{
	if (userAuthTokenEnabled) {
		[self.manager.client.HTTPHeaders setValue:self.userAuthToken forKey:USER_AUTH_HEADER_VALUE_KEY];
	} else {
		if ([self.manager.client.HTTPHeaders valueForKey:USER_AUTH_HEADER_VALUE_KEY])
			[self.manager.client.HTTPHeaders removeObjectForKey:USER_AUTH_HEADER_VALUE_KEY];
	}

	_userAuthTokenEnabled = userAuthTokenEnabled;
}

- (void)setApiKeyEnabled:(BOOL)apiKeyEnabled
{
	if (apiKeyEnabled) {
		[self.manager.client.HTTPHeaders setValue:@"" forKey:API_HEADER_VALUE_KEY];
	} else {
		if ([self.manager.client.HTTPHeaders valueForKey:API_HEADER_VALUE_KEY])
			[self.manager.client.HTTPHeaders removeObjectForKey:API_HEADER_VALUE_KEY];
	}
	_userAuthTokenEnabled = apiKeyEnabled;
}

#pragma mark - Public Class Callback Methods

+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObjectsDictionary:(NSDictionary *)dictionary successBlock:(void(^)(NSDictionary *))aSuccessBlock
{
    aSuccessBlock(dictionary);
}

+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObject:(id)object successBlock:(void(^)(id))aSuccessBlock
{
    aSuccessBlock(object);
}

+ (void) objectLoader:(RKObjectLoader *)objectLoader didLoadObjects:(NSArray *)objects successBlock:(void(^)(NSArray *))aSuccessBlock
{
    aSuccessBlock(objects);
}

+ (void) objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error failureBlock:(void(^)(NSError *))aFailureBlock
{
    aFailureBlock(error);
}

@end
