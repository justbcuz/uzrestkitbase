//
//  UZObjectMappingProvider.h
//  UZDataModel
//
//  Created by Justin Leger on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <RestKit/RestKit.h>

#import "FSCategory.h"
#import "UZBout.h"
#import "UZBoutTarget.h"
#import "UZCategory.h"
#import "UZFriendRequest.h"
#import "UZMeta.h"
#import "UZErrorMeta.h"
#import "UZObject.h"
#import "UZPlace.h"
#import "UZTarget.h"
#import "UZTargetType.h"
#import "UZUser.h"
#import "UZUserSelf.h"
#import "UZValueObject.h"

@interface UZObjectMappingProvider : RKObjectMappingProvider

@end
