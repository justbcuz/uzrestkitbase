//
//  main.m
//  UZRestKitBase
//
//  Created by Justin Leger on 4/18/12.
//  Copyright (c) 2012 Leger Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
