//
//  RKTest.m
//  UZDataModel
//
//  Created by Justin Leger on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RKTest.h"

#import <CoreLocation/CoreLocation.h>
#import <RestKit/RestKit.h>

#import "UZUser+Services.h"
#import "UZUserSelf+Services.h"
#import "UZPlace+Service.h"

@interface RKTest()

@property (nonatomic, retain) CLLocationManager *locationManager;

-(void) afterLogin:(UZUserSelf *) aUser;

@end

@implementation RKTest

@synthesize locationManager;

//- (id)init {
//    self = [super init];
//	if (self) {
//        
//    }
//    
//	return self;
//}

-(void) dealloc
{
	[locationManager release];
	[super dealloc];
}

-(void) runRKTests
{	
//	[[self locationManager] startUpdatingLocation];
	
	__block __typeof__(self) bself = self;
	
    [UZUserSelf loginWithUsername:@"user107" andPassword:@"password"
     
					 successBlock:^(NSDictionary * response) {
						 UZUserSelf * me = (UZUserSelf *) [response objectForKey:@"self"];
						 if (me) [UZServiceManager sharedInstance].userAuthToken = me.authToken;
						 [bself afterLogin:me];
						 NSLog(@"response: %@", response.description);
						 NSLog(@"Auth Token: %@", me.authToken);
					 }
     
					 failureBlock:^(NSError * error) {
						 NSLog(@"Log In Failed. Error: %@", error.description);
					 }
     ];
	
	//    [UZUserSelf loginWithAuthToken:nil 
	//                   successBlock:^(NSDictionary * response) {
	//                       UZUserSelf * me = (UZUserSelf *) [response objectForKey:@"self"];
	//                       NSLog(@"response: %@", response.description);
	//                       NSLog(@"Auth Token: %@", me.authToken);
	//                   }
	//					failureBlock:^(NSError * error) {
	//						NSLog(@"Log In Failed. Error: %@", error.userInfo.description);
	//                   }
	//     ];
}

-(void) afterLogin:(UZUserSelf *) aUser
{
	[aUser friendsWithSuccessBlock:^(NSArray * response) {
		NSLog(@"Friends Count: %i", [response count]);
	}
	 
					  failureBlock:^(NSError * error) {
						  NSLog(@"GET Friends Failed. Error: %@", error.userInfo.description);
					  }
	 ];
	
	[UZPlace nearbyPlacesAtLocation:nil withinRadius:0
					   successBlock:^(NSArray * response) {
						   NSLog(@"Friends Count: %i", [response count]);
					   }
	 
	 
					   failureBlock:^(NSError * error) {
						   NSLog(@"GET Friends Failed. Error: %@", error.userInfo.description);
					   }
	 ];

}

- (CLLocationManager *)locationManager
{
	if (locationManager != nil) {
		return locationManager;
	}
	
	locationManager = [[CLLocationManager alloc] init];
	[locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
	[locationManager setDelegate:self];
	
	return locationManager;
}

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation {
	
	[[self locationManager] stopUpdatingLocation];
	
	[UZPlace nearbyPlacesAtLocation:newLocation withinRadius:0
					   successBlock:^(NSArray * response) {
						   NSLog(@"Place Count: %i", [response count]);
					   }

					   
					   failureBlock:^(NSError * error) {
						   NSLog(@"GET Friends Failed. Error: %@", error.userInfo.description);
					   }
	 ];
}

@end
