//
//  UZUser+Services.h
//  RestKitTest
//
//  Created by Justin Leger on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UZServiceManager.h"

#import "UZUser.h"
#import "UZUserSelf.h"

@interface UZUser (Services)

+ (void) userWithKey:(NSString *)aKey successBlock:(void(^)(UZUser *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) friendsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) challengesWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) friendRequestsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) activitiesWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) targetsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) commentsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

@end
