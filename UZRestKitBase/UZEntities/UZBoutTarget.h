//
//  UZBoutTarget.h
//  UZDataModel
//
//  Created by Justin Leger on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZValueObject.h"
#import "UZTarget.h"
#import "UZBout.h"

@interface UZBoutTarget : UZValueObject

@property (nonatomic, retain) NSString* boutKey;
@property (nonatomic, retain) NSString* targetKey;
@property (nonatomic, retain) NSString* resultType;
@property (nonatomic, retain) NSString* result;
@property (nonatomic, retain) NSNumber* duration;
@property (nonatomic, retain) NSNumber* targetPointValue;
@property (nonatomic, retain) NSNumber* xLoc;
@property (nonatomic, retain) NSNumber* yLoc;
@property (nonatomic, retain) NSNumber* zLoc;

@property (nonatomic, assign) BOOL killedUser;
@property (nonatomic, retain) NSNumber* userHitCount;
@property (nonatomic, retain) NSNumber* headHitCount;
@property (nonatomic, retain) NSNumber* torsoHitCount;
@property (nonatomic, retain) NSNumber* armHitCount;
@property (nonatomic, retain) NSNumber* legHitCount;
@property (nonatomic, retain) NSNumber* lifespan;
@property (nonatomic, retain) NSNumber* remainingHealth;
@property (nonatomic, assign) BOOL crippledAtDeath;
@property (nonatomic, retain) NSNumber* pointsEarned;

#pragma mark -
#pragma mark Relationships

@property (nonatomic, retain) UZBout* bout;
@property (nonatomic, retain) UZTarget* target;

@end
