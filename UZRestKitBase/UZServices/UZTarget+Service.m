//
//  UZTarget+Service.m
//  UrbanZombie
//
//  Created by Justin Leger on 4/17/12.
//  Copyright (c) 2012 Leger Incorporated. All rights reserved.
//

#import "UZTarget+Service.h"
#import "UZServiceManager.h"


@implementation UZTarget (Service)

+ (void) targetWithKey:(NSString *)aKey successBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/target/%@", aKey];
	
	//__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			UZTarget * target = (UZTarget *) [dictionary objectForKey:@"target"];
			aSuccessBlock(target);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}


#pragma mark - Place Service Methods

- (void) placeAtPlace:(UZPlace *)aPlace forUser:(UZUser *)aUser successBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[self placeAtPlaceKey:aPlace.identifier forUserKey:aUser.identifier successBlock:aSuccessBlock failureBlock:aFailureBlock];
}

- (void) placeAtPlaceKey:(NSString *)aPlaceKey forUserKey:(NSString *)aUserKey successBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	// Need to do a POST here
	
	//[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
	//RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
}


#pragma mark - Pick Up Service Methods

- (void) pickupAtLoction:(CLLocation *)aLocation successBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	// TODO: Need biz logic making sure we are in the right location
	// TODO: replace self with result
	
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
	
	NSNumber * latitude = [NSNumber numberWithDouble:aLocation.coordinate.latitude];
	NSNumber * longitude = [NSNumber numberWithDouble:aLocation.coordinate.longitude];
	NSMutableDictionary * queryParams = [[NSMutableDictionary alloc] initWithObjectsAndKeys:latitude, @"lat", longitude, @"lng", nil];
    
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/target/%@/pickup", self.identifier];
	resourcePath = [resourcePath stringByAppendingQueryParameters:queryParams];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			UZTarget * target = (UZTarget *) [dictionary objectForKey:@"target"];
			aSuccessBlock(target);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

- (void) reclaimWithSuccessBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	// TODO: Need biz logic for loss of points
	// TODO: replace self with result
	
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/target/%@/reclaim", self.identifier];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			UZTarget * target = (UZTarget *) [dictionary objectForKey:@"target"];
			aSuccessBlock(target);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}


#pragma mark - Update Service Methods

- (void) updateWithSuccessBlock:(void(^)(UZTarget *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	
}

#pragma mark - Comment Service Methods

- (void) commentsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock
{
	[UZServiceManager sharedInstance].userAuthTokenEnabled = YES;
    RKObjectManager * manager  = [UZServiceManager sharedInstance].manager;
    
    //Build resourse path wirth user key
    NSString * userKey = [self isMemberOfClass:[UZUserSelf class]] ? @"self" : self.identifier;
	NSString * resourcePath = [NSString stringWithFormat:@"/v1/users/%@/comments", userKey];
    
    //__block __typeof__(self) bself = self;
    [manager loadObjectsAtResourcePath:resourcePath usingBlock:^(RKObjectLoader* loader) {
        
        // Define the Callback blocks
        
        loader.onDidLoadObjectsDictionary  = ^(NSDictionary * dictionary) {
			NSArray * friends = (NSArray *) [dictionary objectForKey:@"comments"];
			aSuccessBlock(friends);
        };
        
        loader.onDidFailWithError = ^(NSError * error) {
            [UZServiceManager objectLoader:loader didFailWithError:error failureBlock:aFailureBlock];
        };
    }];
}

@end
