//
//  UZTarget.h
//  UZDataModel
//
//  Created by Justin Leger on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZValueObject.h"
#import "UZTargetType.h"
#import "UZUser.h"
#import "UZPlace.h"

@interface UZTarget : UZValueObject

@property (nonatomic, retain) NSString* typeKey;
@property (nonatomic, retain) NSString* userKey;
@property (nonatomic, retain) NSString* targetUserKey;
@property (nonatomic, retain) NSString* currentPlaceKey;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* gender;
@property (nonatomic, retain) NSNumber* height;
@property (nonatomic, retain) NSNumber* respawnDuration;
@property (nonatomic, retain) NSNumber* speedFactor;
@property (nonatomic, retain) NSNumber* winCount;
@property (nonatomic, retain) NSNumber* lossCount;
@property (nonatomic, retain) NSNumber* totalPoint;
@property (nonatomic, retain) NSNumber* currentPoint;
@property (nonatomic, retain) NSDate* lastBout;
@property (nonatomic, retain) NSDate* lastLoss;
@property (nonatomic, assign) BOOL placed;

@property (nonatomic, retain) NSNumber* userKillCount;
@property (nonatomic, retain) NSNumber* userHitCount;
@property (nonatomic, retain) NSNumber* headHitCount;
@property (nonatomic, retain) NSNumber* torsoHitCount;
@property (nonatomic, retain) NSNumber* armHitCount;
@property (nonatomic, retain) NSNumber* legHitCount;
@property (nonatomic, retain) NSNumber* averageLifespan;
@property (nonatomic, retain) NSNumber* lastHeath;

@property (nonatomic, retain) NSString* thumbSmall;
@property (nonatomic, retain) NSString* thumbMedium;
@property (nonatomic, retain) NSString* thumbLarge;

#pragma mark -
#pragma mark Relationships

@property (nonatomic, retain) UZTargetType* targetType;

@property (nonatomic, retain) UZUser* user;
@property (nonatomic, retain) UZPlace* place;
@property (nonatomic, retain) UZUser* targetUser;
@property (nonatomic, retain) NSSet* bouttargets;

@end
