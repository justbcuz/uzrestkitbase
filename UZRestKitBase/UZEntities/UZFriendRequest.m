//
//  UZFriendRequest.m
//  UZDataModel
//
//  Created by Justin Leger on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZFriendRequest.h"

@implementation UZFriendRequest

@synthesize userKey;
@synthesize requestedUserKey;
@synthesize confirmationState;
@synthesize confirmationKey;

@synthesize user;
@synthesize requestedUser;


#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping * _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZFriendRequest Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZFriendRequest class]] retain];
        //_mapping.primaryKeyAttribute = @"identifier";
        
        [_mapping mapKeyPathsToAttributes:
         
		 @"key", @"identifier",
		 @"inserted", @"createdDate",
		 @"updated", @"updatedDate",
		 
		 @"userKey", @"userKey",
		 @"requestedUserKey", @"requestedUserKey",
		 @"confirmationState", @"confirmationState",
		 @"confirmationKey", @"confirmationKey",
         
         nil];
		
		[_mapping mapRelationship:@"user" withMapping:[UZUser mapping]];
		[_mapping mapRelationship:@"requestedUser" withMapping:[UZUser mapping]];
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{   
    [super initialize];
    
    [UZFriendRequest mapping];
}

@end
