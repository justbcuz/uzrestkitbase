//
//  UZPlace+Service.h
//  UrbanZombie
//
//  Created by Justin Leger on 4/17/12.
//  Copyright (c) 2012 Leger Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "UZPlace.h"

@interface UZPlace (Service)

+ (void) placeWithKey:(NSString *)aKey successBlock:(void(^)(UZPlace *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

+ (void) nearbyPlacesAtLocation:(CLLocation *)aLocation withinRadius:(NSInteger)aRadius successBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

- (void) commentsWithSuccessBlock:(void(^)(NSArray *))aSuccessBlock failureBlock:(void(^)(NSError *))aFailureBlock;

@end
