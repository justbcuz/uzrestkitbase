//
//  UZUser.h
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZValueObject.h"

@interface UZUser : UZValueObject

@property (nonatomic, retain) NSString* facebookID;
@property (nonatomic, retain) NSString* twitterID;

@property (nonatomic, retain) NSString* username;
@property (nonatomic, retain) NSString* gender;

@property (nonatomic, retain) NSString* city;
@property (nonatomic, retain) NSString* state;
@property (nonatomic, retain) NSString* country;

@property (nonatomic, retain) NSNumber* skillLevel;
@property (nonatomic, retain) NSNumber* checkinCooldown;
@property (nonatomic, retain) NSNumber* maxBoutLength;
@property (nonatomic, retain) NSNumber* killCount;
@property (nonatomic, retain) NSNumber* winCount;
@property (nonatomic, retain) NSNumber* lossCount;
@property (nonatomic, retain) NSNumber* fleeCount;

@property (nonatomic, retain) NSNumber* headHitCount;
@property (nonatomic, retain) NSNumber* torsoHitCount;
@property (nonatomic, retain) NSNumber* armHitCount;
@property (nonatomic, retain) NSNumber* legHitCount;
@property (nonatomic, retain) NSNumber* totalShots;
@property (nonatomic, retain) NSNumber* totalHits;
@property (nonatomic, retain) NSNumber* totalMisses;
@property (nonatomic, retain) NSNumber* accuracy;

@property (nonatomic, retain) NSNumber* totalPoints;
@property (nonatomic, retain) NSNumber* curentPoints;

@property (nonatomic, retain) NSString* thumbSmall;
@property (nonatomic, retain) NSString* thumbMedium;
@property (nonatomic, retain) NSString* thumbLarge;

@property (nonatomic, retain) NSNumber* sponsorLevel;

#pragma mark -
#pragma mark Relationships

@property (nonatomic, retain) NSSet* bouts;
@property (nonatomic, retain) NSSet* targets;
@property (nonatomic, retain) NSSet* friends;
@property (nonatomic, retain) NSSet* friendsRequests;

#pragma mark -
#pragma mark Convinience getters
@property (nonatomic, readonly) NSString* location;
@property (nonatomic, readonly) NSString* winLoss;

@end
