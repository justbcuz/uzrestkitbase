//
//  UZUser.m
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZUser.h"
#import "UZBout.h"
#import "UZTarget.h"

@implementation UZUser

@synthesize facebookID;
@synthesize twitterID;

@synthesize username;
@synthesize gender;

@synthesize city;
@synthesize state;
@synthesize country;

@synthesize skillLevel;
@synthesize checkinCooldown;
@synthesize maxBoutLength;
@synthesize killCount;
@synthesize winCount;
@synthesize lossCount;
@synthesize fleeCount;

@synthesize headHitCount;
@synthesize torsoHitCount;
@synthesize armHitCount;
@synthesize legHitCount;
@synthesize totalShots;
@synthesize totalHits;
@synthesize totalMisses;
@synthesize accuracy;

@synthesize thumbSmall;
@synthesize thumbMedium;
@synthesize thumbLarge;

@synthesize totalPoints;
@synthesize curentPoints;

@synthesize sponsorLevel;

@synthesize bouts;
@synthesize targets;
@synthesize friends;
@synthesize friendsRequests;


#pragma mark -
#pragma mark Convinience getters

@synthesize location;
- (NSString *) location {
	
	NSString * result = @"";
	if (self.city) result = [result stringByAppendingString:self.city];
	if (self.state) result = [result stringByAppendingString:[NSString stringWithFormat:@", %@", self.state]];
	if (self.country) result = [result stringByAppendingString:[NSString stringWithFormat:@", %@", self.country]];
	
	return result;
}

@synthesize winLoss;
- (NSString *) winLoss {
	
	NSString * result = @"";
	if (self.winCount) result = [result stringByAppendingString:[self.winCount stringValue]];
	if (self.lossCount) result = [result stringByAppendingString:[NSString stringWithFormat:@"/%@", [self.lossCount stringValue]]];
	
	return result;
}


#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping * _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZUser Mapping");
        
        _mapping = [[RKObjectMapping mappingForClass:[UZUser class]] retain];
        //_mapping.primaryKeyAttribute = @"identifier";
        
        [_mapping mapKeyPathsToAttributes:
         
			@"key", @"identifier",
			@"inserted", @"createdDate",
			@"updated", @"updatedDate",
		 
			@"facebookID", @"facebookID",
			@"twitterID", @"twitterID",
		 
			@"un", @"username",
			@"gender", @"gender",

			@"city", @"city",
			@"state", @"state",
			@"country", @"country",

			@"skill", @"skillLevel",
			@"cooldown", @"checkinCooldown",
			@"boutLength", @"maxBoutLength",
			@"kill", @"killCount",
			@"win", @"winCount",
			@"loss", @"lossCount",
			@"flee", @"fleeCount",

			@"headHitCount", @"headHitCount",
			@"torsoHitCount", @"torsoHitCount",
			@"armHitCount", @"armHitCount",
			@"legHitCount", @"legHitCount",
			@"totalHits", @"totalShots",
			@"totalHits", @"totalHits",
			@"totalMisses", @"totalMisses",
			@"accuracy", @"accuracy",

			@"thumbSmall", @"thumbSmall",
			@"thumbMedium", @"thumbMedium",
			@"thumbLarge", @"thumbLarge",

			@"total", @"totalPoints",
			@"current", @"curentPoints",

			@"sponsorLevel", @"sponsorLevel",
			 
		 nil];
		
        [_mapping mapRelationship:@"bouts" withMapping:[UZBout mapping]];
        [_mapping mapRelationship:@"targets" withMapping:[UZTarget mapping]];
        [_mapping mapRelationship:@"friends" withMapping:[UZUser mapping]];
        [_mapping mapRelationship:@"friendReuests" withMapping:[UZUser mapping]];
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{   
    [super initialize];
    
    [UZUser mapping];
}

@end
