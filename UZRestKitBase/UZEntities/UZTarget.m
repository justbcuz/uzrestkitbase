//
//  UZTarget.m
//  RestKitTest
//
//  Created by Justin Leger on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UZTarget.h"
#import "UZBoutTarget.h"

@implementation UZTarget

@synthesize typeKey;
@synthesize userKey;
@synthesize targetUserKey;
@synthesize currentPlaceKey;
@synthesize name;
@synthesize gender;
@synthesize height;
@synthesize respawnDuration;
@synthesize speedFactor;
@synthesize winCount;
@synthesize lossCount;
@synthesize totalPoint;
@synthesize currentPoint;
@synthesize lastBout;
@synthesize lastLoss;
@synthesize placed;

@synthesize userKillCount;
@synthesize userHitCount;
@synthesize headHitCount;
@synthesize torsoHitCount;
@synthesize armHitCount;
@synthesize legHitCount;
@synthesize averageLifespan;
@synthesize lastHeath;

@synthesize thumbSmall;
@synthesize thumbMedium;
@synthesize thumbLarge;

@synthesize targetType;

@synthesize user;
@synthesize place;
@synthesize targetUser;
@synthesize bouttargets;


#pragma mark -
#pragma mark RestKit Mapping Builder

static RKObjectMapping * _mapping = nil;

+ (RKObjectMapping *) mapping {
    
    if (!_mapping) {
        
        NSLog(@"I am building The UZTarget Mapping");
		
        _mapping = [[RKObjectMapping mappingForClass:[UZTarget class]] retain];
        //_mapping.primaryKeyAttribute = @"identifier";
        
        [_mapping mapKeyPathsToAttributes:
         
		 @"key", @"identifier",
		 @"inserted", @"createdDate",
		 @"updated", @"updatedDate",
		 
		 @"typeKey", @"typeKey",
		 @"userKey", @"userKey",
		 @"targetUserKey", @"targetUserKey",
		 @"currentPlaceKey", @"currentPlaceKey",
		 @"name", @"name",
		 @"gender", @"gender",
		 @"height", @"height",
		 @"respawnDuration", @"respawnDuration",
		 @"speedFactor", @"speedFactor",
		 @"winCount", @"winCount",
		 @"lossCount", @"lossCount",
		 @"totalPoint", @"totalPoint",
		 @"currentPoint", @"currentPoint",
		 @"lastBout", @"lastBout",
		 @"lastLoss", @"lastLoss",
		 @"placed", @"placed",
		 
		 @"kills", @"userKillCount",
		 @"hits", @"userHitCount",
		 @"headHitCount", @"headHitCount",
		 @"torsoHitCount", @"torsoHitCount",
		 @"armHitCount", @"armHitCount",
		 @"legHitCount", @"legHitCount",
		 @"averageLifespan", @"averageLifespan",
		 @"lastHeath", @"lastHeath",
         
         @"thumbSmall", @"thumbSmall",
         @"thumbMedium", @"thumbMedium",
         @"thumbLarge", @"thumbLarge",
		 
         nil];
        
        [_mapping mapRelationship:@"targetType" withMapping:[UZTargetType mapping]];
		
        [_mapping mapRelationship:@"user" withMapping:[UZUser mapping]];
        [_mapping mapRelationship:@"place" withMapping:[UZPlace mapping]];
        [_mapping mapRelationship:@"targetUser" withMapping:[UZUser mapping]];
        [_mapping mapRelationship:@"boutTargets" withMapping:[UZBoutTarget mapping]];
        
    }
    
    return _mapping;
}

#pragma mark -
#pragma Class Initialize

+ (void)initialize
{
    [super initialize];
    
    [UZTarget mapping];
}



@end
